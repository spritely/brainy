;;; Copyright 2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.


(define-module (brainy monads)
  #:use-module (ice-9 match)
  #:use-module (oop goops)
  #:export (bind unpack flatten nary-unpacking))

;; Monads
;; ------

;; Base methods for bind, unpack, flatten
(define-method (bind thing function)
  (flatten (unpack thing function)))

(define-method (unpack object function)
  (function object))

(define-method (flatten object)
  object)

(define (nary-unpacking function)
  (lambda args
    (define loop
      (match-lambda*
        ('() function)
        ((thing rest ...)
         (bind thing
               (lambda (arg)
                 (loop rest
                       (lambda remaining
                         (apply function (cons arg remaining)))))))))
    (loop args function)))
