#lang racket

(require syrup
         goblins/utils/modyule)

(provide (struct-out unterval)
         (struct-out supported)

         (struct-out contradiction)
         contradictory?

         the-nothing
         nothing?

         brainy-generics-modyule)

;; . o O (Just commit to moving to Syrup records with CATerms?)
(struct unterval (low high) #:transparent)
(struct supported (evidence val) #:transparent)

(struct contradiction (x y)
  #:transparent)
(define contradictory? contradiction?)

(struct nothing ())
(define the-nothing (nothing))

;;; Merge code
(define (brainy-generics-modyule #:generics [_generics #f])
  (define (merge-untervals unter-x unter-y)
    'TODO
    )

  (define (merge-unterval-number unter num)
    (if (and (>= num (unterval-low unter))
             (<= num (unterval-high unter)))
        num
        (contradiction unter num)))

  (define (merge-supported supp-x supp-y)
    (define x-val (supported-val supp-x))
    (define y-val (supported-val supp-y))
    (define merge (generics 'merge))
    (define merged-val (merge x-val y-val))
    (cond
      ;; If the merged value is the very same as the x-val,
      ;; this isn't new information so don't add evidence
      [(eq? merged-val x-val)
       supp-x]
      [(contradictory? merged-val)
       (contradiction supp-x supp-y)]
      [else
       (supported (set-union (supported-evidence supp-x)
                             (supported-evidence supp-y))
                  merged-val)]))

  (define (merge-supported-and-other supp other)
    'TODO)

  ;; @@: Hardly the most efficient way to do this, but does it matter?
  (define merge
    (match-lambda*
      [(list (? unterval? x) (? unterval? y))
       (merge-untervals x y)]
      [(or (list (? unterval? unter) (? number? num))
           (list (? number? num) (? unterval? unter)))
       (merge-unterval-number unter num)]
      [(list (? supported? supp-x) (? supported? supp-y))
       (merge-supported supp-x supp-y)]
      [(or (list (? supported? supp) other)
           (list other (? supported? supp)))
       (merge-supported-and-other supp other)]
      ;; anything and the contradiction is a contradiction
      [(or (list (? contradictory? this-contradiction) _)
           (list _ (? contradictory? this-contradiction)))
       this-contradiction]
      ;; it's somethinge else
      [(list other-x other-y)
       (if ((generics 'equivalent?) other-x other-y)
           ;; it's either the same thing at this point...
           other-x
           ;; or it's a contradiction.
           (contradiction other-x other-y))]))

  (define equivalent? equal?)
  (define-modyule (_brainy-generics-modyule)
    (merge equivalent?))
  ;; allow for inheritance
  (define self (_brainy-generics-modyule))
  (define generics (or _generics self))

  self)


(module+ test

  (define generics (brainy-generics-modyule))

  )
