;;; Copyright 2021-2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (tests test-brainy)
  #:use-module (goblins)
  #:use-module (brainy)
  #:use-module (srfi srfi-64)  ; tests
  )

(test-begin "test-brainy")

(define am (make-actormap))
(define-syntax-rule (prun body ...)
  (actormap-churn-run!
   am (lambda () body ...)
   #:catch-errors? #f))

(define* (fahrenheit-celsius #:key
                             [fahrenheit nothing] [celsius nothing]
                             [thirty-two 32])
  (define-pcell f fahrenheit)
  (define-pcell p32 thirty-two)
  (define c (e:/ (e:* (e:- f p32) 5) 9))
  (add-content c celsius)
  (values f c p32))

(define (e:f2c f)
  (e:/ (e:* (e:- f 32) 5) 9))


;; (define* (p:celsius-kelvin c k)
;;   (define many (spawn-pcell))
;;   ((constant 273.15) many)
;;   (p:+ c many k)
;;   (values celsisus kelvin))

(define-values (test1:f test1:c test1:p32)
  (prun (fahrenheit-celsius #:fahrenheit 113)))

(test-eqv (prun (content test1:f))
  113)
(test-eqv (prun (content test1:c))
  45)
(test-eqv (prun (content test1:p32))
  32)

(define-values (test2:f test2:c test2:p32)
  (prun (fahrenheit-celsius #:celsius 45)))

(test-eqv (prun (content test2:f))
  113)
(test-eqv (prun (content test2:c))
  45)
(test-eqv (prun (content test2:p32))
  32)

(define-values (test3:f test3:c test3:p32)
  (prun (fahrenheit-celsius #:celsius 45
                            #:fahrenheit 113
                            #:thirty-two nothing)))
(test-eqv (prun (content test3:f))
  113)
(test-eqv (prun (content test3:c))
  45)
(test-eqv (prun (content test3:p32))
  32)

;; Make sure p:* and p:/ don't hit divide-by-zero issues on the constraint
(define-values (test4:f test4:c test4:p32)
  (prun (fahrenheit-celsius #:fahrenheit 32)))

(test-eqv (prun (content test4:f))
  32)
(test-eqv (prun (content test4:c))
  0)
(test-eqv (prun (content test4:p32))
  32)

;; f2c works with intervals too
(let ((f2c-interval (prun (content (prun (e:f2c (make-interval 50 90)))))))
  (test-equal (interval-low f2c-interval)
    10)
  (test-equal (interval-high f2c-interval)
    290/9))

;; and in the reverse
(let* ((f (prun (spawn-pcell)))
       (c (prun (e:f2c f))))
  (prun (add-content c (make-interval 10 290/9)))
  (let ((f-interval (prun (content f))))
    (test-equal (interval-low f-interval)
      50)
    (test-equal (interval-high f-interval)
      90)))

;; TODO: Figure out why this doesn't work and fix.  It makes
;;   a nonsensical interval:
;; (let ((f2c-interval (prun (content (prun (e:f2c (make-interval 10 90)))))))
;;   (test-equal (interval-low f2c-interval)
;;     10)
;;   (test-equal (interval-high f2c-interval)
;;     290/9))

(define (similar-triangles s-ba h-ba s h)
  (let ((ratio (spawn-pcell)))
    (p:* s-ba ratio h-ba)
    (p:* s ratio h)))

(define building-height (prun (spawn-pcell)))
(define building-shadow (prun (spawn-pcell)))
(define barometer-height (prun (spawn-pcell)))
(define barometer-shadow (prun (spawn-pcell)))
(prun
 (similar-triangles barometer-shadow barometer-height
                    building-shadow building-height))

;; Add shadows information first
(prun (add-content building-shadow (make-interval 54.9 55.1)))
(prun (add-content barometer-height (make-interval 0.3 0.32)))
(prun (add-content barometer-shadow (make-interval 0.36 0.37)))

(test-eqv (prun (interval-low (content building-height)))
  44.51351351351351)

(test-eqv (prun (interval-high (content building-height)))
  48.977777777777774)

;; This is straight from Radul's dissertation
(define (fall-duration t h)
  ;; gravity
  (define g (make-interval 9.789 9.832))
  ;; time, squared
  (define t^2 (e:square t))
  ;; gravity times time squared
  (define gt^2 (e:* g t^2))
  ;; height: one half of gravity squared
  ;; (height was already passed in,
  ;;  so we use wiring diagram style)
  (p:* 1/2 gt^2 h))

;; now let's add the fall time
(define fall-time (prun (spawn-pcell)))
(prun (fall-duration fall-time building-height))
(prun (add-content fall-time (make-interval 2.9 3.1)))

;; the lower bound should be the same...
(test-eqv (prun (interval-low (content building-height)))
  44.51351351351351)
;; but the upper bound should be more precise
(test-eqv (prun (interval-high (content building-height)))
  47.24276000000001)

;; but now the network has even refined *our* measurements!
(test-eqv (prun (interval-low (content barometer-height)))
  0.3)     ; stayed the same
(test-eqv (prun (interval-high (content barometer-height)))
  0.3183938287795994) ; used to be 0.32!

;; Add content from switches
(define-values (control1 control2 control3 output)
  (prun
   (define control1 (spawn-pcell))  ; eventually #t
   (define control2 (spawn-pcell))  ; same
   (define control3 (spawn-pcell))  ; eventually #f
   (define output (spawn-pcell))

   (p:switch control1 (make-interval 1 7) output)
   (p:switch control2 (make-interval 3 9) output)
   (p:switch control3 5 output)
   (values control1 control2 control3 output)))

;; At this point, we know nothing about our output
(test-assert (nothing? (prun (content output))))

;; flip the switch, first partial info...
(prun (add-content control1 #t))
(test-assert (pval-equal? (prun (content output))
                          (make-interval 1 7)))

;; next info narrows...
(prun (add-content control2 #t))
(test-assert (pval-equal? (prun (content output))
                          (make-interval 3 7)))

;; but the third one will never flip
(prun (add-content control3 #f))
(test-assert (pval-equal? (prun (content output))
                          (make-interval 3 7)))


(define-syntax-rule (prun-content expr ...)
  (prun (content (prun expr ...))))

(test-equal (prun-content (e:not #t))
  #f)
(test-equal (prun-content (e:not #f))
  #t)
(test-equal (prun-content (e:conditional #t 'yes 'no))
  'yes)
(test-equal (prun-content (e:conditional #f 'yes 'no))
  'no)
(define (try-conditional-route control)
  (define a-cell (prun (spawn-pcell)))
  (define b-cell (prun (spawn-pcell)))
  (prun (p:conditional-router control 'data a-cell b-cell))
  (list (prun (content a-cell))
        (prun (content b-cell))))

(test-equal (try-conditional-route #t)
  (list 'data nothing))
(test-equal (try-conditional-route #f)
  (list nothing 'data))

(let ((foo (prun (spawn-pcell))))
  (define when-result-cell
    (prun
     (e:when foo
       (display "hallelujah!\n")
       (e:* foo 2))))
  ;; the display part was delayed, is captured here
  (test-equal (with-output-to-string
                (lambda ()
                  (prun (add-content foo 21))))
    "hallelujah!\n")
  ;; multiplication result
  (test-equal (prun (content when-result-cell))
    42))

(define (e:factorial n)
  (e:if (e:= 0 n)
        1
        (e:* n (e:factorial (e:- n 1)))))

(test-equal (prun-content (e:factorial 5))
  120)

(test-end "test-brainy")
