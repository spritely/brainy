;;; Copyright 2021-2022 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;; A klugy module to help users get up and running fast.

(define-module (brainy prun)
  #:use-module (system repl common)
  #:use-module (system repl command)
  #:use-module (system repl error-handling)
  #:use-module (goblins)
  #:use-module (brainy)
  #:export (prun))

;; borrowed from guile/system/repl/repl.scm
(define (with-stack-and-prompt thunk)
  (call-with-prompt (default-prompt-tag)
    (lambda () (start-stack #t (thunk)))
    (lambda (k proc)
      (with-stack-and-prompt (lambda () (proc k))))))

;; an actormap, a Goblins low-level transactional heap
(define am (make-actormap))

(define-syntax-rule (prun body ...)
  (actormap-churn-run!
   am (lambda () body ...)
   #:catch-errors? #f))

(define (prun-form-transform form)
  (syntax-case form (define define-values)
    ((define id exp)
     #'(define id
         (prun exp)))
    ((define-values (ids ...) exp)
     #'(define-values (ids ...)
         (prun exp)))
    (exp
     #'(prun exp))))

(define-inlinable (prun-meta-command repl exp)
  (call-with-values
      (lambda ()
        (repl-eval repl (prun-form-transform exp)))
    (lambda l
      (for-each (lambda (v)
                  (repl-print repl v))
                l))))

(define-meta-command ((prun brainy) repl exp)
  "prun EXP

Run EXP in the setup-for-hacking propagator environment"
  (prun-meta-command repl exp))
